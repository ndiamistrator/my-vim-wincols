function! WinCols(...) abort
    if a:0 && !empty(a:1)
        if type(a:1) == type('')
            let cols = max([1, str2nr(a:1)])
        else
            let cols = max([1, float2nr(a:1)])
        endif
    else
        let cols = 2
    endif
    let nwin = winnr('$')
    let cols = min([cols, nwin])
    while 1
        let div = nwin / cols
        if !empty(div)
            break
        endif
        let cols -= 1
    endwhile
    let spare = nwin - div * cols
    let bufs = tabpagebuflist()
    let i = 1
    exe 'tabnew +buf\ '..bufs[0]
    let winminheight = &winminheight
    let winminwidth = &winminwidth
    let &winminheight = 0
    let &winminwidth = 0
    try
        for buf in bufs[1:]
            if !empty(i % div)
                exe 'belowright split +buf\ '..buf
                let i += 1
            elseif !empty(spare)
                exe 'belowright split +buf\ '..buf
                let spare -= 1
            else
                exe 'botright vertical split +buf\ '..buf
                let i += 1
            endif
            exe 'wincmd ='
        endfor
    finally
        let &winminheight = winminheight
        let &winminwidth = winminwidth
    endtry
endfunction

command! -nargs=? -bar WinCols call WinCols(<q-args>)
